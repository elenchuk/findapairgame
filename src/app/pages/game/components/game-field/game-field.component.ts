import {ChangeDetectionStrategy, ChangeDetectorRef, Component} from '@angular/core';
import {HttpService} from '../../../../core/services/http.service';
import {CardInterface} from '../../../../core/interfaces/card.interface';
import {take} from 'rxjs';
import {ScoreService} from '../../../../core/services/score.service';
import {stageHideAnimation} from '../../../../core/animations/card.animations';
import {CardStateEnum} from '../../../../core/enums/game.enum';

@Component({
  selector: 'app-game-field',
  templateUrl: './game-field.component.html',
  styleUrls: ['./game-field.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  animations: [stageHideAnimation]
})
export class GameFieldComponent {
  public data: CardInterface[] = [];
  public isGameStart: boolean = false;
  public isGameOver: boolean = false;
  public interval: number | undefined;
  public gameTimerCount: number = 0;
  public preGameTimerCount: number = 5;
  private compareArray: CardInterface[] = [];
  private foundIndexesArray: number[] = [];

  constructor(
    private httpService: HttpService,
    private cdr: ChangeDetectorRef,
    private scoreService: ScoreService
  ) {
    this.startGame();
  }

  public startGame(): void {
    this.httpService.getData().pipe(
      take(1)
    ).subscribe((data: CardInterface[]) => {
      this.data = data;
      this.isGameStart = true;
      this.isGameOver = !this.isGameStart;
      this.gameTimerCount = 0;
      this.cdr.detectChanges();
    });
  }

  public selectCard(card: CardInterface): void {
    if (card.state === CardStateEnum.DEFAULT && !this.foundIndexesArray.includes(card.id)) {
      this.markCard(card);
      this.checkPares();
      this.cdr.detectChanges();
    }
  }

  public hideCards(): void {
    if (!this.isGameOver) {
      this.preGameTimerCount = 5;
      const timer: number = 1000;
      const preGameTimer = setInterval(() => {
        this.preGameTimerCount--;
        if (this.preGameTimerCount === 0) {
          this.data.forEach(item => {
            item.state = CardStateEnum.DEFAULT;
          });
          this.startTimer();
          clearInterval(preGameTimer);
        }
        this.cdr.detectChanges()
      }, timer);
    }
  }

  private checkPares(): void {
    if (this.compareArray.length > 1) {
      if (this.compareArray[0].id === this.compareArray[1].id) {
        this.markSuccessPares();
      } else {
        this.resetWrongPares();
      }
    }
  }

  private startTimer(): void {
    const timer: number = 1000;
    this.interval = window.setInterval(() => {
      this.gameTimerCount++;
      this.cdr.detectChanges();
    }, timer)
  }

  private markCard(card: CardInterface): void {
    card.state = card.state === CardStateEnum.FLIPPED ? CardStateEnum.DEFAULT : CardStateEnum.FLIPPED;
    this.compareArray.push(card);
  }

  private markSuccessPares(): void {
    for (let item of this.compareArray) {
      item.state = CardStateEnum.FLIPPED;
    }
    this.foundIndexesArray.push(this.compareArray[0].id);
    this.compareArray = [];
    this.checkIsGameOver();
  }

  private resetWrongPares(): void {
    const timer: number = 500;
    setTimeout(() => {
      for (let item of this.compareArray) {
        item.state = CardStateEnum.DEFAULT;
      }
      this.compareArray = [];
    }, timer)
  }

  private checkIsGameOver(): void {
    let uniqueCardsLength: number = this.data.length / 2;
    if (this.foundIndexesArray.length === uniqueCardsLength) {
      this.foundIndexesArray = [];
      this.compareArray = [];
      this.isGameOver = true;
      clearInterval(this.interval);
      this.scoreService.setScore(String(this.gameTimerCount));
      this.cdr.detectChanges();
    }
  }
}
