import {ChangeDetectionStrategy, Component, HostListener, Input} from '@angular/core';
import {cardHoverAnimation, flipAnimation} from '../../../../core/animations/card.animations';

@Component({
  selector: 'app-game-card',
  templateUrl: './game-card.component.html',
  styleUrls: ['./game-card.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  animations: [flipAnimation, cardHoverAnimation]
})
export class GameCardComponent {
  @Input() public cardState: string = '';
  @Input() public cardImg: string = '';
  public isHover: boolean = false;

  constructor() {
  }

  @HostListener('mouseenter')
  onMouseEnter() {
    this.isHover = true;
  }

  @HostListener('mouseleave')
  onMouseLeave() {
    this.isHover = false;
  }

}
