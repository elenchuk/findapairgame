import {NgModule} from '@angular/core';
import {GameFieldComponent} from './components/game-field/game-field.component';
import {SharedModule} from '../../shared/shared.module';
import {NgForOf, NgIf, NgStyle} from '@angular/common';
import {GameCardComponent} from './components/game-card/game-card.component';
import {GameComponent} from './game.component';
import {GameRoutingModule} from './game-routing.module';

@NgModule({
  declarations: [
    GameComponent,
    GameFieldComponent,
    GameCardComponent
  ],
  imports: [
    GameRoutingModule,
    SharedModule,
    NgForOf,
    NgIf,
    NgStyle
  ],
  exports: [
    GameFieldComponent
  ]
})

export class GameModule {

}
