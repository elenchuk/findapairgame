import {ChangeDetectionStrategy, ChangeDetectorRef, Component, OnDestroy, OnInit} from '@angular/core';
import {ScoreService} from '../../../core/services/score.service';
import {ScoreInterface} from '../../../core/interfaces/score.interface';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-score',
  templateUrl: './score.component.html',
  styleUrls: ['./score.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ScoreComponent implements OnInit, OnDestroy {
  public bestScore: string = '';
  public lastScore: string = '';
  private scoreData$: Subscription | undefined;

  constructor(
    private scoreService: ScoreService,
    private cdr: ChangeDetectorRef,
  ) {
  }

  ngOnInit(): void {
    this.scoreData$ = this.scoreService.getScore().subscribe((score: ScoreInterface) => {
      this.bestScore = score.bestScore;
      this.lastScore = score.lastScore;
      this.cdr.detectChanges();
    })
  }

  ngOnDestroy() {
    this.scoreData$?.unsubscribe();
  }

}
