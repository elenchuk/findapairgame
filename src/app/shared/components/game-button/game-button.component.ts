import {ChangeDetectionStrategy, Component, Input} from '@angular/core';

@Component({
  selector: 'app-game-button',
  templateUrl: './game-button.component.html',
  styleUrls: ['./game-button.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class GameButtonComponent {
  @Input() text: string = '';
}
