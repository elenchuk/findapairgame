export enum CardStateEnum {
  DEFAULT = 'default',
  FLIPPED = 'flipped'
}

export enum ScoreEnum {
  BEST_SCORE = 'bestScore',
  LAST_SCORE = 'lastScore',
}
