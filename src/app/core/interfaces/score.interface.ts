export interface ScoreInterface {
  bestScore: string;
  lastScore: string;
}
