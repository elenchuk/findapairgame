export interface CardInterface {
  id: number;
  image: string;
  state: string;
}
