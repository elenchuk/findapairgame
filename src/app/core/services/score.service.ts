import {Injectable} from '@angular/core';
import {BehaviorSubject, Observable} from 'rxjs';
import {ScoreInterface} from '../interfaces/score.interface';
import {ScoreEnum} from '../enums/game.enum';

@Injectable({
  providedIn: 'root'
})
export class ScoreService {
  private scoreValue$: BehaviorSubject<ScoreInterface> = new BehaviorSubject<ScoreInterface>({
    bestScore: localStorage.getItem(ScoreEnum.BEST_SCORE) || '0',
    lastScore: localStorage.getItem(ScoreEnum.LAST_SCORE) || '0'
  });

  constructor() {
  }

  public setScore(score: string): void {
    let bestScore: string = localStorage.getItem(ScoreEnum.BEST_SCORE) || score;
    if (+score <= +bestScore) {
      bestScore = score;
      localStorage.setItem(ScoreEnum.BEST_SCORE, score);
    }
    localStorage.setItem(ScoreEnum.LAST_SCORE, score);
    this.scoreValue$.next({
      bestScore,
      lastScore: score
    })
  }

  public getScore(): Observable<ScoreInterface> {
    return this.scoreValue$.asObservable();
  }
}
