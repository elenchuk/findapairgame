import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {catchError, map, Observable, of} from 'rxjs';
import {CardInterface} from '../interfaces/card.interface';
import {CardStateEnum} from '../enums/game.enum';

@Injectable({
  providedIn: 'root'
})
export class HttpService {
  private dataPath: string = './assets/data/data.json'

  constructor(
    private http: HttpClient
  ) {
  }

  public getData(): Observable<CardInterface[]> {
    return this.http.get(this.dataPath).pipe(
      map((data: any) => this.mapData(data)),
      map((data: CardInterface[]) => this.shuffleCards(data)),
      catchError(() => of([] as CardInterface[]))
    );
  }

  private mapData(data: any[]): CardInterface[] {
    return data.map((item: any) => ({
      id: item['id'],
      image: item['image'],
      state: CardStateEnum.FLIPPED
    }))
      .filter((item: CardInterface, index: number, array: CardInterface[]) => this.isDuplicate(item, index, array))
  }

  private isDuplicate(item: CardInterface, index: number, array: CardInterface[]): boolean {
    return index === array.findIndex(
      other => item.id === other.id
    )
  }

  private shuffleCards(data: CardInterface[]): CardInterface[] {
    const copyArray = JSON.parse(JSON.stringify(data));
    return data
      .concat(copyArray)
      .sort((a: CardInterface, b: CardInterface) => 0.5 - Math.random());
  }
}
