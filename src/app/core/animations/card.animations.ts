import {
  animate,
  animateChild,
  animation,
  AnimationTriggerMetadata,
  query,
  stagger,
  state,
  style,
  transition,
  trigger,
  useAnimation
} from '@angular/animations';

export const flipAnimation: AnimationTriggerMetadata = trigger('flipAnimation', [
  state('default', style({
    transform: 'none'
  })),
  state('flipped', style({
    transform: 'rotateY(180deg)'
  })),
  transition('default => flipped', [
    animate('400ms')
  ]),
  transition('flipped => default', [
    animate('200ms')
  ])
]);

export const flip = animation([
  style({transform: '{{startTransform}}'}),
  animate('{{duration}}s', style({transform: '{{endTransform}}'})),
]);

export const stageHideAnimation: AnimationTriggerMetadata = trigger('stageHideAnimation', [
  transition(':enter', [
    query('@*', [
      animateChild(),
      style({
        transform: 'none'
      }),
      stagger(50, [
        useAnimation(flip, {
          params: {
            startTransform: 'none',
            endTransform: 'rotateY(181deg)',
            duration: 0.3,
          },
        })
      ])
    ], {optional: true})
  ])
]);

export const cardHoverAnimation: AnimationTriggerMetadata = trigger('cardHoverAnimation', [
  state('true', style({
    boxShadow: '0px 0px 10px 0px rgba(0,0,0,0.75)'
  })),
  state('false', style({
    boxShadow: '*'
  })),
  transition('true => false', [
    animate('400ms')
  ]),
  transition('false => true', [
    animate('200ms')
  ])
])
